/**
* @ngdoc module
* @name ethOpenurlInterlibraryModule
*
* @description
*
* Customization of the "how to getit" section:<br>
* - add link to an interlibrary loan form, if page is a service page (openurl)
*
* <b>AngularJS Dependencies</b><br>
* Service /ja/services/eth-config.service.js {@link ETH.ethConfigService}<br>
*
* <b>CSS Dependencies</b><br>
* CSS /css/eth-openurl-interlibrary.css
*
*/
import {ethConfigService} from './services/eth-config.service';
import {ethOpenurlInterlibraryConfig} from './eth-openurl-interlibrary.config';
import {ethOpenurlInterlibraryController} from './eth-openurl-interlibrary.controller';
import {ethOpenurlInterlibraryHtml} from './eth-openurl-interlibrary.html';

export const ethOpenurlInterlibraryModule = angular
    .module('ethOpenurlInterlibraryModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethOpenurlInterlibraryConfig', ethOpenurlInterlibraryConfig)
        .controller('ethOpenurlInterlibraryController', ethOpenurlInterlibraryController)
        .component('ethOpenurlInterlibraryComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethOpenurlInterlibraryController',
            template: ethOpenurlInterlibraryHtml
        })
