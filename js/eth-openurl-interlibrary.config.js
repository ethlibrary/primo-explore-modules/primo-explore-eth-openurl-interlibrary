export const ethOpenurlInterlibraryConfig = function(){
    return {
        label: {
            text:{
                de: 'Bestellen Sie eine Kopie des Artikels per',
                en: 'Order a copy of the article via'
            },
            linktext:{
                de: 'Fernleihe',
                en: 'Interlibrary loan'
            }
        },
        url:{
            link: {
                de: 'https://mylibrary/ill/form',
                en: 'https://mylibrary/ill/form'
            }
        }
    }
}
