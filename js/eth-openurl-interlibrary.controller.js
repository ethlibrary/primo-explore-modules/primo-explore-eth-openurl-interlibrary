export class ethOpenurlInterlibraryController {

    constructor( $location, ethConfigService, ethOpenurlInterlibraryConfig ) {
        this.$location = $location;
        this.ethConfigService = ethConfigService;
        this.config = ethOpenurlInterlibraryConfig;
    }

    $onInit() {
        try{
            this.openurl = false;
            this.qs = '?';
            if (!this.$location.$$path) {
                console.error("***ETH*** ethOpenurlInterlibraryController.$onInit: $$path not available");
                return;
            }
            if(this.$location.$$path.indexOf('openurl')>-1){
                let s = this.$location.$$search;
                this.openurl = true;

                let oQs = {};
                if(s['rft.jtitle']){
                    oQs = {
                        'jtitle': s['rft.jtitle'],
                        'au': s['rft.aufirst'] + ' ' + s['rft.aulast'],
                        'atitle': s['rft.atitle'],
                        'date': s['rft.date'],
                        'issn': s['rft.issn'],
                        'volume': s['rft.volume'],
                        'pages': s['rft.pages'],
                        'rft_id': s['rft_id']
                    }
                }
                else if(s['title']){
                    let pages = '';
                    if(s['pages'] && s['pages'] != ''){
                        pages = s['pages'];
                    }
                    else if(s['spages'] && s['epages']){
                        pages = s['spages'] + '-' + s['epages'];
                    }

                    let issnOrIsbn = '';
                    if(s['issn'] && s['issn'] != ''){
                        issnOrIsbn = s['issn'];
                    }
                    else if(s['isbn'] && s['isbn'] != ''){
                        issnOrIsbn = s['isbn'];
                    }

                    oQs = {
                        'jtitle': s['title'],
                        'au': s['aufirst'] + ' ' + s['aulast'],
                        'atitle': s['atitle'],
                        'date': s['date'],
                        'issn': issnOrIsbn,
                        'volume': s['volume'],
                        'pages': pages,
                        'rft_id': s['id'],
                    }
                }
                let aQs = [];
                for (const item in oQs) {
                    if(oQs[item]){
                        aQs.push(item + '=' + encodeURIComponent(oQs[item]));
                    }
                }
                this.qs += aQs.join('&');
                document.querySelector('alma-htgi-svc').classList.add('eth-servicepage');
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethOpenurlInterlibraryController $onInit\n\n");
            console.error(e.message);
        }
    }
}

ethOpenurlInterlibraryController.$inject = ['$location', 'ethConfigService', 'ethOpenurlInterlibraryConfig' ];
