import 'primo-explore-eth-openurl-interlibrary';

var app = angular.module('viewCustom', ['ethOpenurlInterlibraryModule']);

app.component('almaHtgiSvcAfter',  {
        template: `
            <eth-openurl-interlibrary-component after-ctrl="$ctrl"></eth-openurl-interlibrary-component>
            `
})
