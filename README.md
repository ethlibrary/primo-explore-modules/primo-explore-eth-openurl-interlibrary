# primo-explore-eth-openurl-interlibrary

## Description

This Module adds an interlibrary loan link to the openURL servicepage. Otherwise the openURL service page will be a dead end, if the resource is not available in the own library.

## Installation

1. Assuming you've installed and are using [primo-explore-devenv](https://github.com/ExLibrisGroup/primo-explore-devenv).

2. Navigate to your view root directory. For example:
    ```
    cd primo-explore/custom/MY_VIEW_ID
    ```
3. If you do not already have a package.json file in this directory, create one:
    ```
    npm init -y
    ```
4. Install this package:
    ```
    npm install primo-explore-eth-openurl-interlibrary --save-dev
    ```

## Usage

Once installed, inject `ethOpenurlInterlibraryModule` as a dependency, and then add the eth-openurl-interlibrary-component directive to the almaHtgiSvcAfter component.

```js

import 'primo-explore-eth-openurl-interlibrary';

var app = angular.module('viewCustom', ['ethOpenurlInterlibraryModule']);

app.component('almaHtgiSvcAfter',  {
        template: `
            <eth-openurl-interlibrary-component after-ctrl="$ctrl"></eth-openurl-interlibrary-component>
            `
})

```
### Config

You'll need to configure the module by adding the url of your interlibrary loan form to eth-openurl-interlibrary.config.js. The querystring including the open url parameters is added by the module.
You can change your ill form in a way that this parameters are filled to the form fields.

```js

url:{
    link: {
        de: 'https://mylibrary/ill/form',
        en: 'https://mylibrary/ill/form'
    }
}

```
